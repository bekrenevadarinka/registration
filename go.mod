module registration

go 1.17

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/lib/pq v1.10.4 // indirect
)
