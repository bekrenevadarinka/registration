package api

import "registration/storage"

type Config struct {
	//Storage configs
	Storage *storage.Config
}

func NewConfig() *Config {
	return &Config{
		Storage: storage.NewConfig(),
	}
}
