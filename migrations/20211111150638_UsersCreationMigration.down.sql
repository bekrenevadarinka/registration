DROP SCHEMA registration;

DROP TABLE registration.users;

DROP TABLE registration.address;
DROP TABLE registration.phone;
DROP TABLE registration.sex;
DROP TABLE registration.roles;
DROP TABLE registration.log;