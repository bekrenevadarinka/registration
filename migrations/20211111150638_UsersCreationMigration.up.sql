CREATE SCHEMA registration AUTHORIZATION postgres;

CREATE TABLE registration.users (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	login varchar(70) NOT NULL,
	last_name varchar(70) NOT NULL,
	first_name varchar(70) NOT NULL,
	surname varchar(70) NULL,
	birt_date date NOT NULL,
	e_mail varchar(300) NULL,
	"password" varchar(30) NOT NULL,
	id_sex int4 NOT NULL,
	id_address int4 NOT NULL,
	id_phone int4 NOT NULL,
	id_role int4 NOT NULL,
	status bool NOT NULL,
	created_at timestamptz NOT NULL DEFAULT CURRENT_DATE,
	update_at timestamptz NULL,
	CONSTRAINT users_pkey PRIMARY KEY (id),
	CONSTRAINT users_un UNIQUE (login)
);

CREATE TABLE registration.address (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	country varchar(70) NOT NULL,
	city varchar(70) NOT NULL,
	street varchar(70) NULL,
	house varchar(10) NOT NULL,
	flat int4 NULL,
	created_at timestamp NOT NULL DEFAULT CURRENT_DATE,
	updated_at timestamp NULL,
	id_user int4 NOT NULL,
	status bool NOT NULL,
	CONSTRAINT address_pkey PRIMARY KEY (id),
	CONSTRAINT addres_fk FOREIGN KEY (id_user) REFERENCES registration.users(id) ON DELETE CASCADE ON UPDATE RESTRICT
);

CREATE TABLE registration.phone (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	number_phone varchar(15) NOT NULL,
	created_at timestamp NOT NULL DEFAULT CURRENT_DATE,
	updated_at timestamp NULL,
	id_user int4 NOT NULL,
	status bool NOT NULL,
	CONSTRAINT chk_number_phone CHECK (((number_phone)::text !~~ '([0-9][0-9][0-9]) [0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]'::text)),
	CONSTRAINT phone_pkey PRIMARY KEY (id),
	CONSTRAINT phone_fk FOREIGN KEY (id_user) REFERENCES registration.users(id) ON DELETE CASCADE ON UPDATE RESTRICT
);

CREATE TABLE registration.roles (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"name" varchar(70) NOT NULL,
	updated_at timestamp NULL,
	created_at timestamp NOT NULL DEFAULT CURRENT_DATE,
	id_user int4 NOT NULL,
	status bool NOT NULL,
	CONSTRAINT roles_pkey PRIMARY KEY (id),
	CONSTRAINT roles_fk FOREIGN KEY (id_user) REFERENCES registration.users(id) ON DELETE CASCADE ON UPDATE RESTRICT
);

CREATE TABLE registration.sex (
	id int4 NOT NULL,
	"name" varchar(30) NOT NULL,
	created_at timestamp NOT NULL DEFAULT CURRENT_DATE,
	updated_at timestamp NULL,
	id_user int4 NOT NULL,
	status bool NOT NULL,
	CONSTRAINT sex_pkey PRIMARY KEY (id),
	CONSTRAINT sex_fk FOREIGN KEY (id_user) REFERENCES registration.users(id) ON DELETE CASCADE ON UPDATE RESTRICT
);

CREATE TABLE registration.log (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	request varchar(50) NOT NULL,
	respons varchar(50) NOT NULL,
	id_type int4 NOT NULL,
	id_user int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT CURRENT_DATE,
	http_user_agent varchar(1000) NOT NULL,
	CONSTRAINT log_pkey PRIMARY KEY (id),
	CONSTRAINT log_fk FOREIGN KEY (id_user) REFERENCES registration.users(id) ON DELETE CASCADE ON UPDATE RESTRICT
	);


