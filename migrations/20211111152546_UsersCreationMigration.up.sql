CREATE TABLE registration.type (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"type" varchar(50) NOT NULL,
	description varchar(50) NOT NULL,
	id_user int4 NOT NULL,
	created_at timestamp NOT NULL DEFAULT CURRENT_DATE,
	updated_at timestamp NULL,
	status bool NOT NULL,
	CONSTRAINT type_pkey PRIMARY KEY (id)
);